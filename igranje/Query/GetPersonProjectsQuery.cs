﻿using System;
using MediatR;
using igranje.Response;

namespace igranje.Query
{
    public class GetPersonProjectsQuery : IRequest<PersonProjectsResponse>
    {
        public Guid PersonId { get; }
        
        public GetPersonProjectsQuery(Guid personId)
        {
            PersonId = personId;
        }
    }
}