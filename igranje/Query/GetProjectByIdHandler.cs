﻿using MediatR;
using igranje.Mapping;
using System.Threading;
using igranje.Response;
using igranje.Repository;
using System.Threading.Tasks;

namespace igranje.Query
{
    public class GetProjectByIdHandler : IRequestHandler<GetProjectByIdQuery, ProjectResponse>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;

        public GetProjectByIdHandler(IProjectRepository projectRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _mapper = mapper;
        }

        public async Task<ProjectResponse> Handle(GetProjectByIdQuery request, CancellationToken cancellationToken)
        {
            var project = await _projectRepository.GetProjectAsync(request.ProjectId);
            return project == null ? null : _mapper.MapProjectDtoToProjectResponse(project);
        }
    }
}