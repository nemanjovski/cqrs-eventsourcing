﻿using MediatR;
using System.Linq;
using igranje.Mapping;
using System.Threading;
using igranje.Response;
using igranje.Repository;
using System.Threading.Tasks;

namespace igranje.Query
{
    public class GetPersonProjectsHandler : IRequestHandler<GetPersonProjectsQuery, PersonProjectsResponse>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;

        public GetPersonProjectsHandler(IProjectRepository projectRepository, IMapper mapper)
        {
            _projectRepository = projectRepository;
            _mapper = mapper;
        }

        public async Task<PersonProjectsResponse> Handle(GetPersonProjectsQuery request, CancellationToken cancellationToken)
        {
            var projects = await _projectRepository.GetProjectsAsync();
            var personProjects = projects.Where(x => x.PersonId == request.PersonId).ToList();
            var personProjectsResponse = new PersonProjectsResponse
            {
                PersonId = request.PersonId,
                Projects = _mapper.MapProjectDtosToProjectResponses(personProjects)
            };
            return personProjectsResponse;
        }
    }
}