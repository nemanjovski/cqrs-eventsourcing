﻿using MediatR;
using igranje.Mapping;
using igranje.Response;
using System.Threading;
using igranje.Repository;
using System.Threading.Tasks;

namespace igranje.Query
{
    public class GetPersonByIdHandler : IRequestHandler<GetPersonByIdQuery, PersonResponse>
    {
        private readonly IMapper _mapper;
        private readonly IPersonRepository _personRepository;

        public GetPersonByIdHandler(IMapper mapper, IPersonRepository personRepository)
        {
            _mapper = mapper;
            _personRepository = personRepository;
        }

        public async Task<PersonResponse> Handle(GetPersonByIdQuery request, CancellationToken cancellationToken)
        {
            // Domain Validation
            var personDto = await _personRepository.GetPersonAsync(request.PersonId);

            return personDto == null ? null : _mapper.MapPersonDtoToPersonResponse(personDto);
        }
    }
}