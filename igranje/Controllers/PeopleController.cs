﻿using System;
using MediatR;
using igranje.Query;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace igranje.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PeopleController(IMediator mediator)
        { 
            _mediator = mediator;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetPeople()
        {
            // API validation
            // Permissions 
            var query = new GetAllPeopleQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }

        [HttpGet("{personId}")]
        public async Task<IActionResult> GetPerson(Guid personId)
        {
            var query = new GetPersonByIdQuery(personId);
            var result = await _mediator.Send(query);
            return result != null ? (IActionResult) Ok(result) : NotFound();
        }
        
        [HttpGet("{personId}/projects")]
        public async Task<IActionResult> GetPersonProjects(Guid personId)
        {
            var query = new GetPersonProjectsQuery(personId);
            var result = await _mediator.Send(query);
            return Ok(result);
        }
    }
}