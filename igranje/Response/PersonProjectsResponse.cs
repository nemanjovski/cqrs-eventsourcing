﻿using System;
using System.Collections.Generic;

namespace igranje.Response
{
    public class PersonProjectsResponse
    {
        public Guid PersonId { get; set; }
        
        public List<ProjectResponse> Projects { get; set; }
    }
}