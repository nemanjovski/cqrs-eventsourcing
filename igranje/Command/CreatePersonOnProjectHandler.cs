﻿using MediatR;
using igranje.Mapping;
using System.Threading;
using igranje.Response;
using igranje.Repository;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace igranje.Command
{
    public class CreatePersonOnProjectHandler
    {
        public class CreateCustomerOrderHandler : IRequestHandler<CreatePersonOnProjectCommand, ProjectResponse>
        {
            private readonly IProjectRepository _projectRepository;
            private readonly IMapper _mapper;
            // private readonly ILogger<CreateCustomerOrderHandler> _logger;

            public CreateCustomerOrderHandler(IProjectRepository projectRepository, IMapper mapper)
            {
                // _logger = logger;
                _projectRepository = projectRepository;
                _mapper = mapper;
            }

            public async Task<ProjectResponse> Handle(CreatePersonOnProjectCommand request, CancellationToken cancellationToken)
            {
                var project = await _projectRepository.CreateProjectAsync(request.PersonId, request.ProductId);
                // _logger.LogInformation($"Created order for customer: {order.CustomerId} for product: {order.ProductId}");
                return _mapper.MapProjectDtoToProjectResponse(project);
            }
        }
    }
}