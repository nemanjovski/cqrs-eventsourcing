﻿using System;
using System.Linq;
using igranje.Dto;
using igranje.Response;
using igranje.Repository;
using System.Collections.Generic;

namespace igranje.Mapping
{
    public class Mapper : IMapper
    {
        private readonly IPersonRepository _personRepository;

        public Mapper(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }
        public List<PersonResponse> MapPersonDtosToPersonResponses(List<PersonDto> dtos)
        {
            return dtos.Select(x => new PersonResponse
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }

        public PersonResponse MapPersonDtoToPersonResponse(PersonDto personDto)
        {
            return new PersonResponse { Id = personDto.Id, Name = personDto.Name };
        }
        
        public List<ProjectResponse> MapProjectDtosToProjectResponses(List<ProjectDto> personProjects)
        {
            return personProjects.Select(x => new ProjectResponse
            {
                Id = x.Id,
                Person = MapPersonDtoToPersonResponse(_personRepository.GetPersonAsync(x.PersonId).GetAwaiter().GetResult()),
                Product = new ProductResponse
                {
                    Id = x.ProductId,
                    Name = "Profi Software",
                    ReleaseDate = DateTime.UtcNow
                },
                Archived = x.Archived,
                GoLiveDate = x.GoLiveDate
            }).ToList();
        }

        public ProjectResponse MapProjectDtoToProjectResponse(ProjectDto project)
        {
            return new ProjectResponse
            {
                Id = project.Id,
                Person = MapPersonDtoToPersonResponse(_personRepository.GetPersonAsync(project.PersonId).GetAwaiter().GetResult()),
                Product = new ProductResponse
                {
                    Id = project.ProductId,
                    Name = "Turing's Machine",
                    ReleaseDate = DateTime.UtcNow
                },
                Archived = project.Archived,
                GoLiveDate = project.GoLiveDate
            };
        }
    }
}
