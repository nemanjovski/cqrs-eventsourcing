﻿using System;

namespace igranje.Dto
{
    public class ProjectDto
    {
        public Guid Id { get; set; }

        public Guid ProductId { get; set; }

        public Guid PersonId { get; set; }

        public DateTime GoLiveDate { get; set; }

        public bool Archived { get; set; }
    }
}
