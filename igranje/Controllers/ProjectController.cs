﻿using System;
using MediatR;
using igranje.Query;
using igranje.Command;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace igranje.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProjectController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateProject([FromBody] CreatePersonOnProjectCommand command)
        {
            var result = await _mediator.Send(command);
            return CreatedAtAction("GetProject", new {project = result.Id}, result);
        }
        
        [HttpGet("{projectId}")]
        public async Task<IActionResult> GetProject([FromRoute]Guid projectId)
        {
            var query = new GetProjectByIdQuery(projectId);
            var result = await _mediator.Send(query);
            return result != null ? (IActionResult) Ok(result) : NotFound();
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllProjects()
        {
            var query = new GetAllProjectsQuery();
            var result = await _mediator.Send(query);
            return Ok(result);
        }
    }
}