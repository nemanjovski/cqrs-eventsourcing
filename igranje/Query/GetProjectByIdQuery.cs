﻿using System;
using MediatR;
using igranje.Response;

namespace igranje.Query
{
    public class GetProjectByIdQuery : IRequest<ProjectResponse>
    {
        public Guid ProjectId { get; }
        
        public GetProjectByIdQuery(Guid projectId)
        {
            ProjectId = projectId;
        }
    }
}