﻿using System;
using System.Linq;
using igranje.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace igranje.Repository
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly List<ProjectDto> _projects = new List<ProjectDto>
        {
            new ProjectDto
            {
                Id = Guid.Parse("4f5d76b8-d38a-40b0-a00d-70e21f3b1dc3"),
                GoLiveDate = DateTime.UtcNow,
                ProductId = Guid.Parse("128b5464-91fc-40c5-b8c3-986dac54047f"),
                PersonId = Guid.Parse("5c2b39ab-d15e-4648-afa1-94c905410110"),
                Archived = false
            }
        };

        public Task<List<ProjectDto>> GetProjectsAsync()
        {
            return Task.FromResult(_projects);
        }

        public Task<ProjectDto> CreateProjectAsync(Guid personId, Guid productId)
        {
            var project = new ProjectDto
            {
                Id = Guid.NewGuid(),
                Archived = false,
                GoLiveDate = DateTime.UtcNow, //2020-10-08T17:31:57.6544166Z
                PersonId = personId,
                ProductId = productId
            }; 
            _projects.Add(project);
            return Task.FromResult(project);
        }

        public Task<ProjectDto> GetProjectAsync(Guid orderId)
        {
            return Task.FromResult(_projects.SingleOrDefault(x => x.Id == orderId));
        }
    }
}