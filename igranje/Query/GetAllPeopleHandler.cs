﻿using MediatR;
using igranje.Mapping;
using System.Threading;
using igranje.Response;
using igranje.Repository;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace igranje.Query
{
    public class GetAllPeopleHandler : IRequestHandler<GetAllPeopleQuery, List<PersonResponse>>
    {
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public GetAllPeopleHandler(IPersonRepository personRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _mapper = mapper;
        }

        public async Task<List<PersonResponse>> Handle(GetAllPeopleQuery request, CancellationToken cancellationToken)
        {
            // Domain Validation
            var personDto = await _personRepository.GetPeopleAsync();
            /////
            ///
            ///
            ///
            ///
            ///
            /// 
            
            return _mapper.MapPersonDtosToPersonResponses(personDto);
        }
    }
}
