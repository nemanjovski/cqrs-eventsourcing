# Od dodatnih paketa, preko NuGet-a , korisceni su sledeci:
    ### mediatR - 8.1.0
    ### MediatR.Exstensions.Microsoft.DependencyInjection - 8.1.0
    ### AspNet.Core.RouteAnalyzer - 0.5.3 // nije neophodan, za rad applikacije
    ### FluentValidation - 9.3.0-preview2
    ### FluentValidation.DependencyInjectionExtensions - 9.3.0-preview2
    ### Swashbuckle.AspNetCore - 5.6.3

    !Note: Verzije paketa, nisu toliko bitne, ovo su poslednji release-ovi za sada za njih.