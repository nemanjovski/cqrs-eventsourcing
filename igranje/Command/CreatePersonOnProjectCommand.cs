﻿using System;
using MediatR;
using igranje.Response;

namespace igranje.Command
{
    public class CreatePersonOnProjectCommand : IRequest<ProjectResponse>
    {
        public Guid PersonId { get; set; }

        public Guid ProductId { get; set; }
    }
}