﻿using MediatR;
using igranje.Response;
using System.Collections.Generic;

namespace igranje.Query
{
    public class GetAllProjectsQuery : IRequest<List<ProjectResponse>> {}
}