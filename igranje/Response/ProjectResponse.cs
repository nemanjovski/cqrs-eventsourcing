﻿using System;

namespace igranje.Response
{
    public class ProjectResponse
    {
        public Guid Id { get; set; }

        public ProductResponse Product { get; set; }

        public PersonResponse Person { get; set; }

        public DateTime GoLiveDate { get; set; }

        public bool Archived { get; set; }
    }
}