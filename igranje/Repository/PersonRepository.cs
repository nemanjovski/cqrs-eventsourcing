﻿using System;
using System.Linq;
using igranje.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace igranje.Repository
{
    public class PersonRepository : IPersonRepository
    {
        private readonly List<PersonDto> _people = new List<PersonDto>
        {
            new PersonDto {Id = Guid.Parse("5c2b39ab-d15e-4648-afa1-94c905410110"), Name = "Milutin Marinovic"},
            new PersonDto {Id = Guid.Parse("dcd40853-ec58-49ec-bd14-585b52e54176"), Name = "Dejan Popovic"},
            new PersonDto {Id = Guid.Parse("6b698c39-c8aa-4d7b-9bc6-a54682b72575"), Name = "Njegos Petrovic"}
        };
        
        public Task<PersonDto> GetPersonAsync(Guid personId)
        {
            return Task.FromResult(_people.SingleOrDefault(x => x.Id == personId));
        }

        public Task<List<PersonDto>> GetPeopleAsync()
        {
            return Task.FromResult(_people);
        }
    }
}