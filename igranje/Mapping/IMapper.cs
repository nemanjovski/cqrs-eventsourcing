﻿using igranje.Dto;
using igranje.Response;
using System.Collections.Generic;

namespace igranje.Mapping
{
    public interface IMapper
    {
        List<PersonResponse> MapPersonDtosToPersonResponses(List<PersonDto> dtos);

        PersonResponse MapPersonDtoToPersonResponse(PersonDto personDto);
        
        List<ProjectResponse> MapProjectDtosToProjectResponses(List<ProjectDto> personProjects);
        
        ProjectResponse MapProjectDtoToProjectResponse(ProjectDto project);
    }
}