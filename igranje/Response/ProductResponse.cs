﻿using System;

namespace igranje.Response
{
    public class ProductResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public DateTime ReleaseDate { get; set; }
    }
}