﻿using MediatR;
using igranje.Mapping;
using igranje.Response;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using igranje.Repository;

namespace igranje.Query
{
    public class GetAllProjectsHandler : IRequestHandler<GetAllProjectsQuery, List<ProjectResponse>>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IMapper _mapper;
        
        public GetAllProjectsHandler(IProjectRepository projectRepository, IMapper mapper)
        {
            _mapper = mapper;
            _projectRepository = projectRepository;
        }
        
        public async Task<List<ProjectResponse>> Handle(GetAllProjectsQuery request, CancellationToken cancellationToken)
        {
            var projects = await _projectRepository.GetProjectsAsync();
            return _mapper.MapProjectDtosToProjectResponses(projects);
        }
    }
}