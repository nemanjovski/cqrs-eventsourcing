﻿using System;

namespace igranje.Response
{
    public class PersonResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}