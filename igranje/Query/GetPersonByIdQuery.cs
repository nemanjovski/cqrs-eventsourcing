﻿using System;
using MediatR;
using igranje.Response;
using System.Collections.Generic;

namespace igranje.Query
{
    public class GetPersonByIdQuery : IRequest <PersonResponse>
    {
        public Guid PersonId { get; }

        public GetPersonByIdQuery(Guid personId)
        {
            PersonId = personId;
        }
    }
}