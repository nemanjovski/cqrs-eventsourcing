﻿using System;
using igranje.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace igranje.Repository
{
    public interface IProjectRepository
    {
        Task<List<ProjectDto>> GetProjectsAsync();
        Task<ProjectDto> CreateProjectAsync(Guid personId, Guid productId);

        Task<ProjectDto> GetProjectAsync(Guid projectId);
    }
}