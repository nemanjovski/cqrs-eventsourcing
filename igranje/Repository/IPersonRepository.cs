﻿using System;
using igranje.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace igranje.Repository
{
    public interface IPersonRepository
    {
        Task<PersonDto> GetPersonAsync(Guid personId);
        
        Task<List<PersonDto>> GetPeopleAsync();
    }
}
